<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h1>Buat Account Baru</h1>
	<h2>Sign Up Form</h2>
    <form action="/welcome" method="POST"> 
    @csrf
			<label for="fname">First name:</label><br><br>
			<input type="text" id="fname" name="fname"><br><br>
			<label for="lname">Last name:</label><br><br>
			<input type="text" id="lname" name="lname"><br>
		    <br>
	<label>Gender:</label><br><br>
			<input type="radio" id="male" name="gender" value="male">
			<label for="male">Male</label><br>
			<input type="radio" id="female" name="gender" value="female">
			<label for="female">Female</label><br>
			<input type="radio" id="other" name="gender" value="other">
			<label for="other">Other</label><br>
		    <br>
	<label>Nationality</label><br><br>
		<select>
			<option value="indonesian">Indonesian</option>
			<option value="singaporean">Sigaporean</option>
			<option value="malaysian">Malaysian</option>
			<option value="australian">Australian</option>
		</select><br><br>
	<label>Language Spoken:</label><br><br>
		<input type="checkbox" name="Bahasa1">
		<label for="Bahasa1">Bahasa Indonesia</label><br>
		<input type="checkbox" name="Bahasa2">
		<label for="Bahasa2">English</label><br>
		<input type="checkbox" name="Bahasa3">
		<label for="Bahasa3">Other</label><br><br>
	<label>Bio:</label><br><br>
	<textarea cols="30" rows="9" id="Bio"></textarea><br>
	<input type="submit" value="Sign Up">
</form>
</body>
</html>